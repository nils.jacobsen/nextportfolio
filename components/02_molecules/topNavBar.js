import styled from 'styled-components'
import { theme } from '../../styles/theme'
import { Menu } from '../01_atoms/font'
import smoothscroll from 'smoothscroll-polyfill'
import { useRef, useEffect } from 'react'

const TopNavBarItem = styled.div`
    display: flex;
`;

const NavPoint = styled.div`
    cursor: pointer;
`;
const FontWrapper = styled.div`
    padding: 4px 24px;
`;
const Underline = styled.div`
    background-color: ${theme.colors.blue};
    height: 2px;
    margin: 0px 20px;
`;

function clickHandler(title){
    document.querySelector('#' + title.toLowerCase()).scrollIntoView({
        behavior: 'smooth'
    });
}

export default function TopNavBar({section}){
    const isAvailable = useRef(false);

    useEffect(() => {
        isAvailable.current = typeof window !== "undefined" && window.location.search;
        if (isAvailable.current != "undefined"){
            smoothscroll.polyfill();
        }   
    }, []);

    return <TopNavBarItem>
        {["PROJECTS","PASSION","CV"].map((sectionTitle, index)=>(
            <NavPoint key={index} onClick={()=>clickHandler(sectionTitle)}>
                <FontWrapper>
                    <Menu color={theme.colors.white} content={sectionTitle}/>
                </FontWrapper>
                { sectionTitle === section && <Underline />}
            </NavPoint>
        ))}
    </TopNavBarItem>
}