import styled from 'styled-components'
import Image from 'next/image'
import { theme } from '../../styles/theme';
import { ObjectTitle, Menu, PageSubtitle } from '../01_atoms/font'; 
import { motion } from 'framer-motion'
import { useState } from 'react';
import { useRouter } from 'next/router';

const variants = {
    pre: { opacity: 0, x: 40 },
    after: { opacity: 1, x: 0 },
}

const ProjectCardItem = styled(motion.div)`
    width: 100%;
    height: 100%;
    border-radius: 10px;
    overflow: hidden;
    -webkit-box-shadow: 0px 37px 50px 5px rgba(0,0,0,0.41); 
    box-shadow: 0px 37px 50px 5px rgba(0,0,0,0.41);
    position: relative;
`;

const MainContent = styled.div`
    width: 100%;
    height: 100%;
    position: relative;
    z-index: ${theme.zIndex.md};
    background: linear-gradient(180deg, rgba(32, 36, 41, 0.34) 0%, rgba(32, 36, 41, 0.34) 66.15%, #17191E 100%);
    border-radius: 10px;
    box-sizing: border-box;
    padding: 24px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

const BottomContainer = styled.div`
`;

const ImageWrapper = styled.div`
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: ${theme.zIndex.xs};

    div{
        width: 100%;
        height: 100%;
    }
    #projectCardImage{
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
`;

export default function ProjectCard({index, content, myWindow, isMoved}){
    
    const [isAfter, setIsAfter] = useState(false);
    const router = useRouter();

    const handleClick = () => {
        if(isMoved !== true){
            router.push("melo");
        }
    }

    return <ProjectCardItem id="projectCard" myWindow={myWindow} onMouseUp={()=>handleClick()} whileHover={{ scale: 1.01 }} variants={variants} transition={{ duration: 0.2 }} animate={ isAfter ? "pre" : "after" }>
        <ImageWrapper>
            <Image id="projectCardImage" draggable="false" width="1000" height="1000" src={content.cover} alt={content.subtitle} />
        </ImageWrapper>
        <MainContent>
            <PageSubtitle content={"0" + (index+1)} color={theme.colors.white}/>
            <BottomContainer>
                <Menu content={content.subtitle} color={theme.colors.white}/>
                <ObjectTitle content={content.title} color={theme.colors.white}/>
            </BottomContainer>
        </MainContent>
    </ProjectCardItem>
}