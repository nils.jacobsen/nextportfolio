import styled from 'styled-components'
import BasicLayout from '../00_utilities/basicLayout'
import TitleSection from '../02_molecules/titleSection'
import HeroCarousel from '../02_molecules/heroCarousel'
import { motion } from 'framer-motion'
import { theme } from '../../styles/theme'

import { useState } from 'react'

const HeroSectionItem = styled.div`
    height: 100vh;
    width: 100vw;
    overflow: hidden;
`;
const TextWrapper = styled(motion.div)`
    padding-top: 5rem;
    pointer-events: none;
`;

export default function HeroSection({setSection}){
    const [slidePos, setSlidePos] = useState(0);

    return (
        <HeroSectionItem id="projects">
            <HeroCarousel setSlidePos={setSlidePos}/>
            <BasicLayout>
                <TextWrapper transition={{ duration: 0.1 }} style={{ opacity: (slidePos/200)+1, x: slidePos/2 }}>
                    <TitleSection isOnFullLayout={true} setSection={setSection} isH1={true} title={"Interaction Designer and Fullstack Developer"} subtitle={"NILS JACOBSEN"}/>
                </TextWrapper>
            </BasicLayout>
        </HeroSectionItem>
    );
}