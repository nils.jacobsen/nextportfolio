import styled from 'styled-components'
import TopNavBar from '../02_molecules/topNavBar'
import { theme } from '../../styles/theme'
import IconButton from '../01_atoms/iconButton';
import { CardTitle, NormalText } from '../01_atoms/font';
import Icon from '../01_atoms/icon';
import { useRouter } from 'next/router';

const TopNavigationItem = styled.div`
    position: fixed;
    height: 80px;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: ${theme.zIndex.lg};
`;
const SocialMediaWrapper = styled.div`
    position: absolute;
    height: 80px;
    padding-right: 24px;
    right: 0;
    display: flex;
    flex-direction: row;
    align-items: center;
`;
const HomeWrapper = styled.div`
    position: absolute;
    height: 80px;
    padding-left: 24px;
    left: 0;
    display: flex;
    flex-direction: row;
    align-items: center;
`;
const TitleWrapper = styled.div`
    position: absolute;
    height: 44px;
    margin-left: 24px;
    box-sizing: border-box;
    left: 0;
    display: flex;
    align-items: center;
    border-radius: 10px;
    background: rgba( 89, 91, 94, 0.7 );
    backdrop-filter: blur( 10.0px );
    -webkit-backdrop-filter: blur( 24.0px );
`;
const Separator = styled.div`
    width: 1px;
    height: 100%;
    background-color: ${theme.colors.grey};
    opacity: 1;
`;
const TitleIconWrapper = styled.div`
    display: flex;
    align-items: center;
    height: 100%;
    padding: 0 16px;
    opacity: 1;
    &:hover{
        background-color: rgba( 200, 200, 200, 0.1 );
    }
`;
const ParagraphWrapper = styled.div`
    display: flex;
    align-items: center;
    height: 100%;
    padding-left: 16px;
    padding-right: 24px;
`;

export default function StaticUITop({section, isMainPage}){
    const router = useRouter();

    if(isMainPage){
        return <TopNavigationItem>
            <TopNavBar section={section}/>
            <SocialMediaWrapper>
                <IconButton link={theme.soicalMedia.gitlab} type="gitlab"/>
                <IconButton link={theme.soicalMedia.linkedIn} type="linkedIn"/>
                <IconButton link={theme.soicalMedia.dribble} type="dribble"/>
            </SocialMediaWrapper>
            <HomeWrapper>
                <IconButton link="/" type="nj"/>
            </HomeWrapper>
        </TopNavigationItem>
    }else{
        return <TopNavigationItem>
            <TitleWrapper>
                <TitleIconWrapper onClick={()=>router.push("/")}>
                    <Icon type="arrowLeft"/>
                </TitleIconWrapper>
                <Separator />
                <ParagraphWrapper>
                    <NormalText content={"Panasonic Image App"} color={theme.colors.white}/>
                </ParagraphWrapper>
            </TitleWrapper>
            <SocialMediaWrapper>
                <IconButton link={theme.soicalMedia.gitlab} type="gitlab"/>
                <IconButton link={theme.soicalMedia.linkedIn} type="linkedIn"/>
                <IconButton link={theme.soicalMedia.dribble} type="dribble"/>
            </SocialMediaWrapper>
        </TopNavigationItem>
    }
}