import styled from 'styled-components'
import Story from '../01_atoms/story';

const StoryListItem = styled.div`
    display: flex;
    gap: 20px;
`;

export default function StoryList(){
    return <StoryListItem>
        <Story path="/pia.png" title={"Ideation"}/>
        <Story path="/melo.png" title={"Research"}/>
        <Story path="/pia.jpg" title={"Prototyp"}/>
    </StoryListItem>
}