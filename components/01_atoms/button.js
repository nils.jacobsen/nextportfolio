import styled from 'styled-components'
import { NormalText } from './font';
import { theme } from '../../styles/theme';

const ButtonItem = styled.div`
    background-color: ${theme.colors.white};
    height: 44px;
    border-radius: 10px;
    display: flex;
    align-items: center;
    padding: 0 24px;
    width: fit-content;
`;

export default function Button({content}){
    return <ButtonItem>
        <NormalText content={content} color={theme.colors.black} />
    </ButtonItem>
}