import styled from 'styled-components'
import Image from "next/image"
import { StoryTitle } from './font';
import { theme } from '../../styles/theme';

const StoryItem = styled.div`
    display: flex;
    flex-direction: column;
    gap: 12px;
    align-items: center;
`;

const Wrapper = styled.div`
    width: 74px;
    height: 74px;
    border-radius: 50%;
    border: 2px solid ${theme.colors.blue};
    display: flex;
    justify-content: center;
    align-items: center;
`;

const StoryPreview = styled.div`
    width: 56px;
    height: 56px;
    border-radius: 50%;
    background-color: ${theme.colors.grey};

    #storyPreviewImage{
        object-fit: cover;
        border-radius: 50%;
    }
`;

export default function Story({path,title}){
    return <StoryItem>
        <Wrapper>
            <StoryPreview>
                <Image id="storyPreviewImage" draggable="false" width="58" height="58" src={path} alt={title} />
            </StoryPreview>
        </Wrapper>
        <StoryTitle content={title} color={theme.colors.lightGrey}/>
    </StoryItem>
}