import styled from 'styled-components'
import BackgroundImage from '../02_molecules/backgroundImage';
import TitleSection from '../02_molecules/titleSection';
import BasicLayout from '../00_utilities/basicLayout';
import { gridCalc } from '../00_utilities/gridCalc';
import { CardText, NormalText, ObjectTitle } from '../01_atoms/font';
import { theme } from '../../styles/theme';
import Image from 'next/image';
import TimeLine from '../02_molecules/timeLine';


const cvContent = {
    "eductaion": [
        {
            "titleShort": "HfG",
            "title": "Hochschule für Gestaltung",
            "locationOrDuration": "Schwäbisch Gmünd",
            "result": "B.A. Interaction Design 2023",
            "date": ""
        },
        {
            "titleShort": "DHBW",
            "title": "Duale Hochschule Ravensburg",
            "locationOrDuration": "Campus Friedrichshafen",
            "result": "B.Eng. Electrical Engineering 2019",
            "date": ""
        }
    ],
    "experience": [
        {
            "titleShort": "",
            "title": "Interaktionswerk",
            "locationOrDuration": "Internship - 5 Month",
            "result": "UX Designer",
            "date": "2021"
        },
        {
            "titleShort": "",
            "title": "Fraunhofer IPA",
            "locationOrDuration": "Working Student - 18 Month",
            "result": "Creative Technologist",
            "date": "2019"
        },
        {
            "titleShort": "",
            "title": "Daimler AG",
            "locationOrDuration": "Bachelor’s Thesis - 3 Month",
            "result": "Creative Technologist, Tooling UI Design",
            "date": "2019"
        },
        {
            "titleShort": "",
            "title": "MBRDNA",
            "locationOrDuration": "Internship Abroad - 4 Month",
            "result": "UX and Advanced Engineering",
            "date": "2019"
        },
        {
            "titleShort": "",
            "title": "Daimler AG",
            "locationOrDuration": "Internship - 3 Month",
            "result": "User Interface Developer",
            "date": "2018"
        },
    ]
}

const CvSectionItem = styled.div`
    height: 100vh;
    width: 100vw;
`;
const TextLayout = styled.div`
    position: relative;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: space-between;
`;
const TextWrapper = styled.div`
    width: ${gridCalc(3,2)};
    height: 100%;
    display: flex;
    flex-direction: column;
    gap: 2rem;
`;
const CvWrapper = styled.div`
    width: ${gridCalc(4,5)};
    height: 100%;
    display: flex;
    justify-content: space-between;
`;
const EducationWrapper = styled.div`
    width: calc((100% - 40px) / 2);
    height: 100%;
    display: flex;
    flex-direction: column;
`;
const ExperienceWrapper = styled.div`
    width: calc((100% - 40px) / 2);
    height: 100%;
    display: flex;
    flex-direction: column;
`;
const ImageWrapper = styled.div`
    width: 36px;
    height: 36px;
    background-color: ${props => props.bgColor};
    border-radius: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 8px;
`;
const ResumeWrapper = styled.div`
    padding-top: 8px;
`;
const TimeLineWrapper = styled.div`
    flex-grow: 1;
`;

export default function CvSection({setSection}){
    return (
        <CvSectionItem id="cv">
            <BackgroundImage isRight={true} path={""} isDarkShape={true}/>
            <BasicLayout>
                <TextLayout>
                    <TextWrapper>
                        <TitleSection isOnFullLayout={false} isH1={false} title={"The practical approach."} subtitle={"CURICULUM VITAE"} setSection={setSection}/>
                        <NormalText content="Since I left school, it’s been always important for me to not only spend my time in lectures during my studies, but to collect some great practical experiences during internships and working student programms." color={theme.colors.lightGrey} />
                    </TextWrapper>
                    <CvWrapper>
                        <EducationWrapper>
                            <ImageWrapper bgColor={theme.colors.bluish}>
                                <Image src="/books.svg" alt={"books"} width="20" height="20" />
                            </ImageWrapper>
                            <ObjectTitle content="Education" color={theme.colors.white}/>
                            <ResumeWrapper>
                                <CardText content="B.A. Interaction Design 2023" color={theme.colors.lightGrey}/>
                                <CardText content="B.Eng. Electrical Engineering 2019" color={theme.colors.lightGrey}/>
                            </ResumeWrapper>
                            <TimeLineWrapper>
                                <TimeLine type="education" data={cvContent.eductaion}/>
                            </TimeLineWrapper>
                        </EducationWrapper>
                        <ExperienceWrapper>
                            <ImageWrapper bgColor={theme.colors.brownish}>
                                <Image src="/bag.svg" alt={"bag"} width="20" height="20" />
                            </ImageWrapper>
                            <ObjectTitle content="Experience" color={theme.colors.white}/>
                            <ResumeWrapper>
                                <CardText content="4 years working experience" color={theme.colors.lightGrey}/>
                                <CardText content="agency / research / automotive" color={theme.colors.lightGrey}/>
                            </ResumeWrapper>
                            <TimeLineWrapper>
                                <TimeLine type="experience" data={cvContent.experience}/>
                            </TimeLineWrapper>
                        </ExperienceWrapper>
                    </CvWrapper>
                </TextLayout>
            </BasicLayout>
        </CvSectionItem>
    );
}