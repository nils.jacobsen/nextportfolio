import styled from 'styled-components'
import { theme } from "./../../styles/theme";

const PageTitleFontH1 = styled.h1`
    font-family: ${theme.fonts.pageTitle.font}, ${theme.fonts.pageTitle.font==='IBM Plex Mono' ? 'monospace' : 'sans-serif'};
    font-size: ${theme.fonts.pageTitle.size};
    font-weight: ${theme.fonts.pageTitle.weight};
    line-height: ${theme.fonts.pageTitle.lineHeight};
    color: ${props => props.color};
    margin: 0;
`;
const PageTitleFontH2 = styled.h2`
    font-family: ${theme.fonts.pageTitle.font}, ${theme.fonts.pageTitle.font==='IBM Plex Mono' ? 'monospace' : 'sans-serif'};
    font-size: ${theme.fonts.pageTitle.size};
    font-weight: ${theme.fonts.pageTitle.weight};
    line-height: ${theme.fonts.pageTitle.lineHeight};
    color: ${props => props.color};
    margin: 0;
`;

export function PageTitle({content,isH1,color}){
    if(isH1===true){
        return <PageTitleFontH1 color={color}>
            {content}
        </PageTitleFontH1>
    }else{
        return <PageTitleFontH2 color={color}>
            {content}
        </PageTitleFontH2>
    }
}

//-----------------------------------------

const PageSubtitleItemH2 = styled.h2`
    font-family: ${theme.fonts.pageSubtitle.font}, ${theme.fonts.pageSubtitle.font==='IBM Plex Mono' ? 'monospace' : 'sans-serif'};
    font-size: ${theme.fonts.pageSubtitle.size};
    font-weight: ${theme.fonts.pageSubtitle.weight};
    line-height: ${theme.fonts.pageSubtitle.lineHeight};
    color: ${props => props.color};
    margin: 0;
`;
const PageSubtitleItemH3 = styled.h3`
    font-family: ${theme.fonts.pageSubtitle.font}, ${theme.fonts.pageSubtitle.font==='IBM Plex Mono' ? 'monospace' : 'sans-serif'};
    font-size: ${theme.fonts.pageSubtitle.size};
    font-weight: ${theme.fonts.pageSubtitle.weight};
    line-height: ${theme.fonts.pageSubtitle.lineHeight};
    color: ${props => props.color};
    margin: 0;
`;

export function PageSubtitle({content,isH2,color}){
    if(isH2===true){
        return <PageSubtitleItemH2 color={color}>
            {content}
        </PageSubtitleItemH2>
    }else{
        return <PageSubtitleItemH3 color={color}>
            {content}
        </PageSubtitleItemH3>
    }
    
}

//-----------------------------------------

const MenuItem = styled.li`
    font-family: ${theme.fonts.menu.font}, ${theme.fonts.menu.font==='IBM Plex Mono' ? 'monospace' : 'sans-serif'};
    font-size: ${theme.fonts.menu.size};
    font-weight: ${theme.fonts.menu.weight};
    line-height: ${theme.fonts.menu.lineHeight};
    color: ${props => props.color};
    margin: 0;
    list-style: none;
`;


export function Menu({content, color}){
    return <MenuItem color={color}>
        {content}
    </MenuItem>
}


//-----------------------------------------

const ObjectTitleItem = styled.li`
    font-family: ${theme.fonts.objectTitle.font}, ${theme.fonts.menu.font==='IBM Plex Mono' ? 'monospace' : 'sans-serif'};
    font-size: ${theme.fonts.objectTitle.size};
    font-weight: ${theme.fonts.objectTitle.weight};
    line-height: ${theme.fonts.objectTitle.lineHeight};
    color: ${props => props.color};
    margin: 0;
    list-style: none;
`;


export function ObjectTitle({content, color}){
    return <ObjectTitleItem color={color}>
        {content}
    </ObjectTitleItem>
}


//-----------------------------------------

const CardTitleItem = styled.li`
    font-family: ${theme.fonts.cardTitle.font}, ${theme.fonts.menu.font==='IBM Plex Mono' ? 'monospace' : 'sans-serif'};
    font-size: ${theme.fonts.cardTitle.size};
    font-weight: ${theme.fonts.cardTitle.weight};
    line-height: ${theme.fonts.cardTitle.lineHeight};
    color: ${props => props.color};
    margin: 0;
    list-style: none;
`;


export function CardTitle({content, color}){
    return <CardTitleItem color={color}>
        {content}
    </CardTitleItem>
}


//-----------------------------------------

const CardTextItem = styled.li`
    font-family: ${theme.fonts.cardText.font}, ${theme.fonts.menu.font==='IBM Plex Mono' ? 'monospace' : 'sans-serif'};
    font-size: ${theme.fonts.cardText.size};
    font-weight: ${theme.fonts.cardText.weight};
    line-height: ${theme.fonts.cardText.lineHeight};
    color: ${props => props.color};
    margin: 0;
    list-style: none;
`;


export function CardText({content, color}){
    return <CardTextItem color={color}>
        {content}
    </CardTextItem>
}


//-----------------------------------------

const NormalTextItem = styled.div`
    font-family: ${theme.fonts.normalText.font}, ${theme.fonts.menu.font==='IBM Plex Mono' ? 'monospace' : 'sans-serif'};
    font-size: ${theme.fonts.normalText.size};
    font-weight: ${theme.fonts.normalText.weight};
    line-height: ${theme.fonts.normalText.lineHeight};
    color: ${props => props.color};
    margin: 0;
`;


export function NormalText({content, color}){
    return <NormalTextItem color={color}>
        {content}
    </NormalTextItem>
}


//-----------------------------------------

const StoryTitleItem = styled.div`
    font-family: ${theme.fonts.storyTitle.font}, ${theme.fonts.storyTitle.font==='IBM Plex Mono' ? 'monospace' : 'sans-serif'};
    font-size: ${theme.fonts.storyTitle.size};
    font-weight: ${theme.fonts.storyTitle.weight};
    line-height: ${theme.fonts.storyTitle.lineHeight};
    color: ${props => props.color};
    margin: 0;
`;


export function StoryTitle({content, color}){
    return <StoryTitleItem color={color}>
        {content}
    </StoryTitleItem>
}