import React from "react";

import { ContextProvider } from "./../01_atoms/motionSliderContext";
import Track from "./../01_atoms/motionSliderTrack";
import Item from "./../01_atoms/motionSliderItem";

const MotionSlider = ({ children, padding, gap, velocity, transition, setSlidePos, setIsMoved }) => {
  return (
    <ContextProvider>
      <Track padding={padding} velocity={velocity} transition={transition} setSlidePos={setSlidePos} setIsMoved={setIsMoved}>
        {children.map((child, i) => (
          <Item key={i} gap={gap} padding={padding}>
            {child}
          </Item>
        ))}
      </Track>
    </ContextProvider>
  );
};

MotionSlider.defaultProps = {
  padding: 0,
  gap: 40,
  velocity: 0.4,
  transition: { type: "spring", damping: 500 }
};

export default MotionSlider;