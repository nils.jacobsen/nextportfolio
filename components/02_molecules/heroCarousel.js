import styled from 'styled-components';
import MotionSlider from './motionSlider';
import { useState, useEffect } from "react";
import ProjectCard from './projectCard';

const projects = [
  {
    title: "Shoot, Edit, Share",
    subtitle: "Panasonic Image App",
    cover: "/pia.png"
  },
  {
    title: "Sound Experience Platform",
    subtitle: "Melo Web App",
    cover: "/melo.png"
  },
  {
    title: "Shoot, Edit, Share",
    subtitle: "Panasonic Image App",
    cover: "/pia.png"
  },
  {
    title: "Shoot, Edit, Share",
    subtitle: "Panasonic Image App",
    cover: "/pia.png"
  },
];

const HeroCarouselItem = styled.div`
  margin-left: ${props => (((props.myWindow.width - (40*7)) / 8) * 3) + (3 * 40)}px;
  width: 50vw;
  height: calc(100vh * 0.7);
  position: absolute;
  top: 0;
  margin-top: calc(100vh * 0.15);
  overflow: visible;
`;

const ProjectCardWrapper = styled.div`
  width: ${props => (((props.myWindow.width - (40*7)) / 8) * 2) + (1 * 40)}px;
  height: 100%;
  border-radius: 10px;
  #projectCard{
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

export default function HeroCarousel({setSlidePos}){
  const [isMoved,setIsMoved] = useState(false);
  const myWindow = GetWindowSize();

  return<HeroCarouselItem myWindow={myWindow}>
      <MotionSlider setIsMoved={setIsMoved} setSlidePos={setSlidePos}>
          {projects.map((project, i) => (
            <ProjectCardWrapper myWindow={myWindow} key={i}>
              <ProjectCard isMoved={isMoved} id="projectCard" index={i} content={project} myWindow={myWindow} />
            </ProjectCardWrapper>
          ))}
      </MotionSlider>
  </HeroCarouselItem>
}

function GetWindowSize() {
    // Initialize state with undefined width/height so server and client renders match
    // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
    const [windowSize, setWindowSize] = useState({
      width: undefined,
      height: undefined,
    });
  
    useEffect(() => {
      // only execute all the code below in client side
      if (typeof window !== 'undefined') {
        // Handler to call on window resize
        function handleResize() {
          // Set window width/height to state
          setWindowSize({
            width: window.innerWidth,
            height: window.innerHeight,
          });
        }
      
        // Add event listener
        window.addEventListener("resize", handleResize);
       
        // Call handler right away so state gets updated with initial window size
        handleResize();
      
        // Remove event listener on cleanup
        return () => window.removeEventListener("resize", handleResize);
      }
    }, []); // Empty array ensures that effect is only run on mount
    return windowSize;
}