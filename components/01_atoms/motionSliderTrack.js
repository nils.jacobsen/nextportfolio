import React, { useContext, useState, useEffect } from "react";
import styled from "styled-components";
import { motion, useAnimation, useMotionValue, useTransform } from "framer-motion";
import useDimensions from "react-use-dimensions";

import { Context } from "./motionSliderContext";

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`;

const StyledTrack = styled(motion.div)`
  display: flex;
  flex-wrap: nowrap;
  min-width: min-content;
  padding-right: ${props => (((props.myWindow.width - (40*7)) / 8) * 1) + (1 * 40)}px;
  cursor: grab;
  &:active {
    cursor: grabbing;
  }
  height: 100%;
`;

const Track = ({ children, padding, velocity, transition, setSlidePos, setIsMoved }) => {

  const [trackRef, trackDimensions] = useDimensions();
  const controls = useAnimation();
  const x = useMotionValue(0);

  function onDrag(event){
    setSlidePos(event.x);
  }

  let myWindow = GetWindowSize();

  const { state, dispatch } = useContext(Context);
  const negativeItems = state.items.map(
    item => item * -1 + trackDimensions.x || 0
  );

  function onDragEnd(event, info) {

    const offset = info.offset.x;
    const correctedVelocity = info.velocity.x * velocity;
    const direction = correctedVelocity < 0 || offset < 0 ? 1 : -1;
    const startPosition = info.point.x - offset;

    const endOffset = direction === 1 ? Math.min(correctedVelocity, offset) : Math.max(correctedVelocity, offset);
    const endPosition = startPosition + endOffset;

    //compute the slots
    const computedSlots = [];
    negativeItems.forEach(element => {
        if(direction === 1){
            computedSlots.push(Math.abs(element-endOffset+(300*state.activeItem)));
        }else{
            computedSlots.push(Math.abs(element-endOffset+(300*(state.activeItem))));
        }
    });

    //the smallest slot is 
    var decides = (acc, cur) => Math.min(acc, cur);
    const closestSlot = computedSlots.reduce(decides);

    //get index om smallest slot
    const activeSlide = computedSlots.indexOf(closestSlot);
    const closestPosition = negativeItems[activeSlide];

    dispatch({ type: "SET_ACTIVE_ITEM", activeItem: activeSlide });

    controls.start({
      x: Math.max(
        closestPosition,
        myWindow.width -
          trackDimensions.width -
          trackDimensions.x || 0
      ),
      transition: { type: "spring", duration: 0.4, mass: 0.2 }
    });
    setTimeout(function() {
      setIsMoved(false);
    }, 50);
  }

  return (
    <Wrapper>
      <StyledTrack
        myWindow={myWindow}
        ref={trackRef}
        padding={padding}
        animate={controls}
        drag="x"
        dragConstraints={{
          left:
            myWindow.width -
            trackDimensions.width -
            trackDimensions.x,
          right: 0 + trackDimensions.x
        }}
        onUpdate={(event)=>onDrag(event)}
        onDragEnd={onDragEnd}
        onDrag={()=>setIsMoved(true)}
      >
        {children}
      </StyledTrack>
    </Wrapper>
  );
};

export default Track;


function GetWindowSize() {

    // Initialize state with undefined width/height so server and client renders match
    // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
    const [windowSize, setWindowSize] = useState({
      width: undefined,
      height: undefined,
    });
  
    useEffect(() => {
      // only execute all the code below in client side
      if (typeof window !== 'undefined') {
        // Handler to call on window resize
        function handleResize() {
          // Set window width/height to state
          setWindowSize({
            width: window.innerWidth,
            height: window.innerHeight,
          });
        }
      
        // Add event listener
        window.addEventListener("resize", handleResize);
       
        // Call handler right away so state gets updated with initial window size
        handleResize();
      
        // Remove event listener on cleanup
        return () => window.removeEventListener("resize", handleResize);
      }
    }, []); // Empty array ensures that effect is only run on mount
    return windowSize;
}