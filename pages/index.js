import Head from 'next/head'
import styled from "styled-components"
import { theme } from "./../styles/theme"
import { useState, useEffect } from "react"
import { useViewportScroll } from 'framer-motion'


import HeroSection from '../components/03_sections/heroSection';
import PassionSection from '../components/03_sections/passionSection';
import CvSection from '../components/03_sections/cvSection';
import StaticUILeft from '../components/04_templates/staticUiLeft';
import StaticUITop from '../components/04_templates/staticUiTop';

const HomeItem = styled.main`
  background-color: ${theme.colors.darkGrey};
`;

export default function Home() {
  const [section, setSection] = useState("");
  const { scrollYProgress } = useViewportScroll()

  return (
    <div>
      <Head>
        <title>Nils Jacobsen</title>
        <meta name="description" content="Nils Jacobsen" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <HomeItem>
        <StaticUILeft section={section}/>
        <StaticUITop section={section} isMainPage={true}/>
        <HeroSection setSection={setSection}/>
        <PassionSection yPos={scrollYProgress} setSection={setSection}/>
        <CvSection setSection={setSection}/>
      </HomeItem>

    </div>
  )
}
