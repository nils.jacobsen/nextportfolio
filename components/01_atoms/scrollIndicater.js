import styled from 'styled-components'
import { motion, useTransform, useViewportScroll } from 'framer-motion'
import { theme } from './../../styles/theme'

const ScrollIndicaterItem = styled.div`
    width: 100px;
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;
const ScrollWrapper = styled.div`
    width: 2px;
    height: 200px;
    background-color: ${theme.colors.grey};
`;
const Indicator = styled(motion.div)`
    width: 2px;
    background-color: ${theme.colors.white};
`;

export default function ScrollIndicater(){
    const { scrollYProgress } = useViewportScroll();
    const height = useTransform(scrollYProgress, [0, 1], [5, 200]);

    return <ScrollIndicaterItem>
        <ScrollWrapper>
            <Indicator style={{ height }}/>
        </ScrollWrapper>
    </ScrollIndicaterItem>
}