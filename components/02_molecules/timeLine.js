import styled from 'styled-components'
import { theme } from '../../styles/theme';
import { CardText, CardTitle, Menu } from '../01_atoms/font';
import { useRef, useLayoutEffect, useState } from 'react';

const TimeLineItem = styled.div`
    width: 100%;
    height: 100%;
    position: relative;
    padding-top: 3rem;
    box-sizing: border-box;
`;
const Line = styled.div`
    margin-top: 6px;
    width: 2px;
    height: ${props => (props.height - props.lastElemHeight) + 'px'};
    margin-left: 1px;
    position: absolute;
    background-color: ${theme.colors.grey};
`;
const PointList = styled.div`
    width: 100%;
    position: absolute;
    display: flex;
    flex-direction: column;
    gap: 40px;
`;
const PointItem = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
`;
const Point = styled.div`
    margin-top: 6px;
    width: 4px;
    height: 4px;
    background-color: ${theme.colors.white};
    border-radius: 2px;
`;
const PointContent = styled.div`
    width: 85%;
    display: flex;
    flex-direction: column;
    gap: 8px;
`;
const PointContentExperience = styled.div`
    width: 85%;
    display: flex;
    flex-direction: column;
    gap: 4px;
`;
const CardTitleWrapper = styled.div`
`;
const CardTitleWrapperExperience = styled.div`
    display: flex;
    align-items: center;
    height: 20px;
    justify-content: space-between;
`;

export default function TimeLine({type, data}){
    const heightRef = useRef();
    const lastElem = useRef();
    const [lineHeight,setLineHeight] = useState(0);
    const [lastElemHeight,setlastElemHeight] = useState(0);


    useLayoutEffect(() => {
        if (heightRef.current) {
            setLineHeight(heightRef.current.offsetHeight);
        }
    }, []);
    useLayoutEffect(() => {
        if (lastElem.current) {
            setlastElemHeight(lastElem.current.offsetHeight);
        }
    }, []);

    if(type==="education"){
        return <TimeLineItem>   
            <Line height={lineHeight} lastElemHeight={lastElemHeight}></Line>
            <PointList ref={heightRef}>
                {data.map((point, i)=>(
                    <PointItem key={i} ref={(i+1)===data.length ? lastElem : undefined}>
                        <Point></Point>
                        <PointContent>
                            <CardTitleWrapper>
                                <CardTitle content={point.titleShort} color={theme.colors.white} />
                                <CardTitle content={point.title} color={theme.colors.white} />
                            </CardTitleWrapper>
                            <Menu content={point.locationOrDuration} color={theme.colors.lightGrey} />
                            <CardText content={point.result} color={theme.colors.white} />
                        </PointContent>
                    </PointItem>
                ))}
            </PointList>
        </TimeLineItem>
    }else{
        return <TimeLineItem>   
            <Line height={lineHeight} lastElemHeight={lastElemHeight}></Line>
            <PointList ref={heightRef}>
                {data.map((point, i)=>(
                    <PointItem key={i} ref={(i+1)===data.length ? lastElem : undefined}>
                        <Point></Point>
                        <PointContentExperience>
                            <CardTitleWrapperExperience>
                                <CardTitle content={point.title} color={theme.colors.white} />
                                <Menu content={point.date} color={theme.colors.lightGrey} />
                            </CardTitleWrapperExperience>
                            <Menu content={point.locationOrDuration} color={theme.colors.lightGrey} />
                            <CardText content={point.result} color={theme.colors.white} />
                        </PointContentExperience>
                    </PointItem>
                ))}
            </PointList>
        </TimeLineItem>
    }  
}