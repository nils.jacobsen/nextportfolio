import styled from 'styled-components'
import Image from 'next/image'
import { theme } from '../../styles/theme';
import { CardTitle, CardText } from '../01_atoms/font';
import { motion } from 'framer-motion';

const variants = {
    pre: { opacity: 0, y: 20 },
    after: { opacity: 1, y: 0 },
}

const PassionCardItem = styled(motion.div)`
    width: calc((100% - 25px) / 2);
    //height: 155px;
    border-radius: 10px;
    background: rgba( 89, 91, 94, 0.7 );
    backdrop-filter: blur( 10.0px );
    -webkit-backdrop-filter: blur( 24.0px );
    box-sizing: border-box;
    padding: 24px 28px;
    display: flex;
    flex-direction: column;
    gap: 8px;
`;
const ImageWrapper = styled.div`
    width: 36px;
    height: 36px;
    background-color: ${props => props.bgColor};
    border-radius: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 8px;
`;

export default function PassionCard({bgColor, img, title, text, isAnimated, index}){
    return <PassionCardItem animate={isAnimated ? "after" : "pre"} variants={variants} transition={{ ease: 'easeOut', delay: (0.1 * index) + 0.15 }}>
        <ImageWrapper bgColor={bgColor}>
            <Image src={"/" + img + ".svg"} alt={img} width="20" height="20" />
        </ImageWrapper>
        <CardTitle content={title} type="cardTitle" color={theme.colors.white}/>
        <CardText content={text} type="cardTitle" color={theme.colors.darkWhite}/>
    </PassionCardItem>
}