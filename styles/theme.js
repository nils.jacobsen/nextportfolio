import { snapshotViewportBox } from "framer-motion";

export const theme = {
    colors: {
        darkGrey: '#393B3E',
        mediumDarkGrey: '#2C2E31',
        grey: '#595D63',
        white: '#FFFFFF',
        blue: '#00D1FF',
        black: '#263040',
        yellow: '#FFF9BC',
        green: '#DEFFBC',
        bluish: '#BCFFFF',
        skin: '#FFD8BC',
        darkWhite: '#C8C4C4',
        brownish: '#FFEAB5',
        lightGrey: '#B4B4B4'
    },
    fonts: {
        pageTitle: {
            font: 'IBM Plex Sans',
            size: '50px',
            weight: 700,
            lineHeight: '65px',
        },
        pageSubtitle: {
            font: 'IBM Plex Mono',
            size: '20px',
            weight: 200,
            lineHeight: '30px',
        },
        menu: {
            font: 'IBM Plex Mono',
            size: '13px',
            weight: 200,
            lineHeight: '20px',
        },
        objectTitle: {
            font: 'IBM Plex Sans',
            size: '25px',
            weight: 600,
            lineHeight: '35px',
        },
        cardTitle: {
            font: 'IBM Plex Sans',
            size: '18px',
            weight: 800,
            lineHeight: '25px',
        },
        cardText: {
            font: 'IBM Plex Sans',
            size: '12px',
            weight: 400,
            lineHeight: '16px',
        },
        normalText: {
            font: 'IBM Plex Sans',
            size: '14px',
            weight: 400,
            lineHeight: '24px',
        },
        storyTitle: {
            font: 'IBM Plex Sans',
            size: '11px',
            weight: 400,
            lineHeight: '15px',
        }
    },
    zIndex: {
        xs: 1000,
        sm: 1001,
        md: 1002,
        lg: 1003,
        xl: 1004
    },
    soicalMedia: {
        linkedIn: 'https://www.linkedin.com/in/nils-jacobsen/',
        dribble: 'https://www.linkedin.com/in/nils-jacobsen/',
        gitlab: 'https://www.linkedin.com/in/nils-jacobsen/',
    }
}