import styled from 'styled-components'
import Image from "next/image"

const IconItem = styled.div`
    width: 24px;
    height: 24px;
    opacity: ${props => props.isdisabled ? 0.5 : 1};
`;

export default function Icon({type, isdisabled}){
    return <IconItem isdisabled={isdisabled}>
        <Image width="500px" height="500px" src={"/assets/" + type + ".svg"} alt={type} />
    </IconItem>
}