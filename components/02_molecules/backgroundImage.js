import styled from 'styled-components'
import Image from "next/image"

const BackgroundImageItem = styled.div`
    width: 100vw;
    height: 100vh;
    position: absolute;
`;
const BackgroundItem = styled.div`
    width: 100vw;
    height: 100vh;
    position: absolute;
    display: flex;
    justify-content: flex-end;
`;
const ImageWrapper = styled.div`
    width: 50%;
    height: 100%;
    object-fit: cover;
    div{
        height: 100%;
    }
    img{
        object-fit: cover;
    }
`;
const BackgroundWrapper = styled.div`
    width: 70%;
    height: 100%;
    object-fit: cover;
    div{
        height: 100%;
    }
    img{
        object-fit: cover;
    }
`;

export default function BackgroundImage({isRight, path, isDarkShape}){
    if(isDarkShape){ 
        return <BackgroundItem>
            <BackgroundWrapper>
                <Image height="1800" width="1787" src="/assets/background.svg" alt="background"></Image>
            </BackgroundWrapper>
        </BackgroundItem>
    }else{
        return <BackgroundImageItem>
            <ImageWrapper>
                <Image width="1436" height="1800" src={"/bigImages/" + path + ".jpg"} alt={path}/>
            </ImageWrapper>
        </BackgroundImageItem>
    }
}