import styled from 'styled-components'
import Icon from './icon'
import { motion } from 'framer-motion'
import { hexToRgba } from './../00_utilities/hexToRgba'
import { theme } from './../../styles/theme'
import Link from 'next/link'

const IconButtonItem = styled.div`
    width: 52px;
    height: 52px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
`;
const Outline = styled(motion.div)`
    width: 52px;
    height: 52px;
    border-radius: 26px;
    border: 1px solid ${props => props.color};
    display: flex;
    justify-content: center;
    align-items: center;
`;
const Inline = styled(motion.div)`
    width: 40px;
    height: 40px;
    border-radius: 20px;
    border: 1px solid ${props => props.color};
    display: flex;
    justify-content: center;
    align-items: center;
    transition: all 0.2s;
    &:hover{
        border: 1px solid ${props=> props.isSelected ? hexToRgba(theme.colors.white,50) : hexToRgba(theme.colors.white,20)};
    }
`;

export default function IconButton({type, isdisabled, isSelected, link}){

    if(link === ""){
        if(isdisabled === true){
            return <IconButtonItem>
                <Icon type={type} isdisabled={true}/>
            </IconButtonItem>
        }else{
            if(isSelected === true){
                return <IconButtonItem>
                    <Outline color={hexToRgba(theme.colors.white,20)}>
                        <Inline 
                            whileTap={{ scale: 0.97 }} 
                            transition={{duration: 0.02}} 
                            color={hexToRgba(theme.colors.white,50)} 
                            isSelected={isSelected}
                        >
                            <Icon type={type} isdisabled={false}/>
                        </Inline>
                    </Outline>
                </IconButtonItem>
            }else{
                return <IconButtonItem>
                    <Outline color={hexToRgba(theme.colors.white,0)}>
                        <Inline 
                            whileTap={{ scale: 0.97 }} 
                            transition={{duration: 0.02}} 
                            color={hexToRgba(theme.colors.white,0)} 
                            isSelected={isSelected}
                        >
                            <Icon type={type} isdisabled={false}/>
                        </Inline>
                    </Outline>
                </IconButtonItem>
            }
        }
    }else{
        if(isdisabled === true){
            return <IconButtonItem>
                <Icon type={type} isdisabled={true}/>
            </IconButtonItem>
        }else{
            if(isSelected === true){
                return <Link href={link} passHref>
                    <IconButtonItem>
                        <Outline color={hexToRgba(theme.colors.white,20)}>
                            <Inline 
                                whileTap={{ scale: 0.97 }} 
                                transition={{duration: 0.02}} 
                                color={hexToRgba(theme.colors.white,50)} 
                                isSelected={isSelected}
                            >
                                <Icon type={type} isdisabled={false}/>
                            </Inline>
                        </Outline>
                    </IconButtonItem>
                </Link>
            }else{
                return <Link href={link} passHref>
                    <IconButtonItem>
                        <Outline color={hexToRgba(theme.colors.white,0)}>
                            <Inline 
                                whileTap={{ scale: 0.97 }} 
                                transition={{duration: 0.02}} 
                                color={hexToRgba(theme.colors.white,0)} 
                                isSelected={isSelected}
                            >
                                <Icon type={type} isdisabled={false}/>
                            </Inline>
                        </Outline>
                    </IconButtonItem>
                </Link>
            }
        }
    }
}