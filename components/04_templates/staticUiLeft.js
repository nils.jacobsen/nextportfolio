import styled from 'styled-components'
import IconButton from '../01_atoms/iconButton'
import ScrollIndicater from '../01_atoms/scrollIndicater'
import { theme } from '../../styles/theme'
import smoothscroll from 'smoothscroll-polyfill'
import { useRef, useEffect } from 'react' 

const StaticUIItem = styled.div`
    position: fixed;
    width: 100px;
    height: 100%;
    z-index: ${theme.zIndex.lg};
`;
const SideWidgets = styled.div`
    position: absolute;
    width: 100px;
    margin-top: 100px;
    padding-top: 48px;
    top: calc(100vh / 2);
    display: flex;
    flex-direction: column;
    align-items: center;
`;
const DownArrowWrapper = styled.div`
    position: absolute;
    width: 100px;
    padding-bottom: 24px;
    bottom: 0;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

function clickDownHandler(title){
    if(title === "PROJECTS"){
        document.querySelector('#passion').scrollIntoView({
            behavior: 'smooth'
        });
    }else{
        document.querySelector('#projects').scrollIntoView({
            behavior: 'smooth'
        });
    }
    
}

export default function StaticUILeft({section}){
    const isAvailable = useRef(false);
    useEffect(() => {
        isAvailable.current = typeof window !== "undefined" && window.location.search;
        if (isAvailable.current != "undefined"){
            smoothscroll.polyfill();
        }   
    }, []);

    return <StaticUIItem>
        <ScrollIndicater />
        <SideWidgets>
            <IconButton link="" type="chat"/>
            <IconButton link="" type="stack"/>
        </SideWidgets>
        <DownArrowWrapper onClick={()=>clickDownHandler(section)}>
            {section !== "PASSION" && <IconButton link="" type={section==="PROJECTS" ? "arrowdown" : "arrowup"}/>}
        </DownArrowWrapper>
    </StaticUIItem>
}