import styled from 'styled-components'
import { PageTitle, PageSubtitle } from '../01_atoms/font';
import { useInView } from 'react-hook-inview'
import { theme } from '../../styles/theme';

const TitleSectionItem = styled.div`
    width: ${props => props.isOnFullLayout ? `calc((((100% - (5 * 40px)) / 6) * 2.5) + (40px * 2))` : `width:100%`};
    position: relative;
    z-index: ${theme.zIndex.md};
`;

export default function TitleSection({isH1, title, subtitle, setSection, isOnFullLayout}){
    const [ref, isVisible] = useInView({ threshold: 0.3, onEnter });

    function onEnter(entry) {
        if(entry.target.children[0].innerText === "NILS JACOBSEN"){
            setSection("PROJECTS");
        }else if(entry.target.children[0].innerText === "CURICULUM VITAE"){
            setSection("CV");
        }else{
            setSection(entry.target.children[0].innerText);
        }   
    }

    return <TitleSectionItem ref={ref} isOnFullLayout={isOnFullLayout}>
        <PageSubtitle content={subtitle} isH2={isH1===true ? true : false} color={'#FFFFFF'}></PageSubtitle>
        <PageTitle content={title} isH1={isH1} color={'#FFFFFF'}></PageTitle>
    </TitleSectionItem>
}