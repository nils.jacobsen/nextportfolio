import styled from 'styled-components'

const BasicLayoutItem = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;
const BasicLayoutContainer = styled.div`
    //Eine Spalte * 6 + (5 * Gap)
    width: calc((((100% - (40px*7)) / 8) * 6) + (5 * 40px));
    height: 70%;
`;

export default function BasicLayout({children}){
    return <BasicLayoutItem>
        <BasicLayoutContainer>
            {children}
        </BasicLayoutContainer>
    </BasicLayoutItem>
}
