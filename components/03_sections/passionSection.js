import styled from 'styled-components'
import BackgroundImage from '../02_molecules/backgroundImage';
import { theme } from '../../styles/theme'
import BasicLayout from '../00_utilities/basicLayout';
import TitleSection from '../02_molecules/titleSection';
import PassionCard from '../02_molecules/passionCard';
import { useEffect, useState, useRef } from 'react';
import { useInView } from 'react-hook-inview'
import StoryList from '../02_molecules/storyList';

const PassionSectionItem = styled.div`
    height: 100vh;
    width: 100vw;
    z-index: ${theme.zIndex.sm};
    position: relative;
`;
const TextLayout = styled.div`
    position: relative;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: flex-end;
`;
const TextWrapper = styled.div`
    width: 55%;
    height: 100%;
    display: flex; 
    flex-direction: column;
    justify-content: flex-end;
    gap: 60px;
`;
const CardWrapper = styled.div`
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    gap: 25px;
`;
const StoryWrapper = styled.div`
    position: absolute;
    width: 50%;
    display: flex;
    justify-content: center;
    bottom: 48px;
`;

export default function PassionSection({setSection, yPos}){
    const [passionBlock, isVisible] = useInView({ threshold: 0.3, onEnter });
    const [isAnimated, setIsAnimated] = useState(false);

    function onEnter(entry) {
        if(isAnimated === false){
            setIsAnimated(true);
        }
    }

    return (
        <PassionSectionItem id="passion">
            <BackgroundImage isRight={false} path={"PassionImage"} isDarkShape={false}/>
            <StoryWrapper>
                <StoryList />
            </StoryWrapper>
            <BasicLayout>
                <TextLayout>
                    <TextWrapper>
                        <TitleSection isOnFullLayout={false} isH1={false} title={"What defines me."} subtitle={"PASSION"} setSection={setSection}/>
                        <CardWrapper ref={passionBlock}>
                            <PassionCard index={0} isAnimated={isAnimated} bgColor={theme.colors.yellow} img="idea" title="Let Room for Ideas" text="Lorem ipsum dolor skh skdh skdhg sdhfsh sit amet, consetetur sadipscing elitr."/>
                            <PassionCard index={1} isAnimated={isAnimated} bgColor={theme.colors.green} img="sustainable" title="Sustainable Design" text="Lorem ipsum dolor skh skdh skdhg sdhfsh sit amet, consetetur sadipscing elitr."/>
                            <PassionCard index={2} isAnimated={isAnimated} bgColor={theme.colors.bluish} img="prototyping" title="Fast Prototyping" text="Lorem ipsum dolor skh skdh skdhg sdhfsh sit amet, consetetur sadipscing elitr."/>
                            <PassionCard index={3} isAnimated={isAnimated} bgColor={theme.colors.skin} img="user" title="Only the Users know" text="Lorem ipsum dolor skh skdh skdhg sdhfsh litr. Lorem ipsum dolor skh skdh skdhg sdhfsh sit amet, consetetur sadipscing elitr."/>
                        </CardWrapper>
                    </TextWrapper>
                </TextLayout>
            </BasicLayout>
        </PassionSectionItem>
    );
}