import styled from 'styled-components'
import { useRouter } from "next/router";
import Image from "next/Image"
import StaticUITop from '../components/04_templates/staticUiTop';
import { theme } from './../styles/theme';
import BackgroundImage from '../components/02_molecules/backgroundImage';
import BasicLayout from '../components/00_utilities/basicLayout';
import Button from '../components/01_atoms/button';
import { NormalText, PageSubtitle, PageTitle } from '../components/01_atoms/font';
import StoryList from '../components/02_molecules/storyList';

const ProjectItem = styled.div`
    height: 100vh;
    width: 100%;
    position: relative;
`;
const ImageWrapper = styled.div`
    background-color: black;
    height: 100vh;
    width: 50%;
`;
const TextLayout = styled.div`
    position: absolute;
    right: calc((((100vw - (40px*7)) / 8) * 1) + (1 * 40px));
    height: 70%;
    width: calc((((100vw - (40px*7)) / 8) * 2.5) + (2 * 40px));
    top: 15%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    gap: 48px;
`;
const ParagraphWrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 24px;
`;
const BottomWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    gap: 48px;
`;

export default function Project(){
    const {
        query: { project },
    } = useRouter();
    return <ProjectItem>
        <StaticUITop isMainPage={false}/>
        <ImageWrapper>
            <BackgroundImage isRight={false} path={project} isDarkShape={false}/>
        </ImageWrapper>
        <TextLayout>
            <StoryList />
            <BottomWrapper>
                <ParagraphWrapper>
                    <PageSubtitle content="PANASONIC IMAGE APP" color={theme.colors.white} />
                    <PageTitle content="Shoot, edit, share" color={theme.colors.white} />
                </ParagraphWrapper>
                <ParagraphWrapper>
                    <NormalText content={"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr."} color={theme.colors.lightGrey}/>
                    <NormalText content={"Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."} color={theme.colors.lightGrey}/>
                </ParagraphWrapper>
                <Button content="See more details"/>
            </BottomWrapper>
        </TextLayout>
    </ProjectItem>
}